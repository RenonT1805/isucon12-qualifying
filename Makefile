export PATH := $(PATH):/home/isucon/local/go/bin

$(eval TIMESTAMP=$(shell date +%Y-%m-%dT%H:%M:%S))
DISCORD_WEBHOOK_LOG = https://discord.com/api/webhooks/995996891819036732/5WAKp6I3XEUZ9i6pOkd6IxB5qlP8r3bb8i3qGXLdBO23RsFWigG8iluI4piu_O017Mna
DISCORD_WEBHOOK_SLOW_QUERY = https://discord.com/api/webhooks/996004567877697536/cRtebzczEEYdWT-EVe_kGoq5PXQ-J7-nB0uE276z-eEx-2c_uPKXY88cpvfE5poyUKZB
DISCORD_WEBHOOK_ACCESS_LOG = https://discord.com/api/webhooks/998578103188209804/d2BxJ1nFvztX60gNKTyqzBqyYPcfpqOEEMTw8AtZFqDvjlsBuVfCw6_l9ZQuLpjqqyB4
DISCORD_WEBHOOK_CICD_LOG = https://discord.com/api/webhooks/998577990009098250/2_Lv4M2PKZD3JRX54IC2nCvMh9nQ_hYqVrWY9aqj8Fmukz31uxGJs_1scU8eenXqqpPp

MYSQL_USER = isucon
MYSQL_PASSWORD = isucon
MYSQL_LONG_QUERY_TIME = 0.5

ISUCON_HOME = /home/isucon
WEBAPP_HOME = /home/isucon/webapp
GO_APP_HOME = /home/isucon/webapp/go
EXEC = isuports

AWS_S3_SLOW_QUERY_DIR = https://s3.console.aws.amazon.com/s3/object/isucon12q-log-storage?region=ap-northeast-1&prefix=slow-query/
AWS_S3_ACCESS_LOG_DIR = https://s3.console.aws.amazon.com/s3/object/isucon12q-log-storage?region=ap-northeast-1&prefix=access-log/
AWS_S3_CI_LOG_DIR = https://s3.console.aws.amazon.com/s3/object/isucon12q-log-storage?region=ap-northeast-1&prefix=ci/

.PHONY: check-status
check-status:
	sudo systemctl status $(EXEC).go.service

.PHONY: setup
setup:
	# pt-query-digest
	sudo apt install -y percona-toolkit
	pt-query-digest --version
	# ALP
	sudo wget https://github.com/tkuchiki/alp/releases/download/v1.0.5/alp_linux_amd64.zip
	unzip alp_linux_amd64.zip
	sudo install ./alp /usr/local/bin
	rm -rf alp alp_linux_amd64
	# pprof
	go get -u runtime/pprof
	go get -u net/http/pprof
	sudo apt install -y graphviz
	# htop
	sudo apt install -y htop
	# awscli
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
	unzip awscliv2.zip
	sudo ./aws/install
	rm awscliv2.zip

.PHONY: pprof
pprof:
	go tool pprof -http=0.0.0.0:1080 /home/isucon/webapp/go/$(EXEC) http://localhost:6060/debug/pprof/profile

.PHONY: slow-on
slow-on:
	echo "set global slow_query_log=1;" | mysql -p$(MYSQL_PASSWORD) -u$(MYSQL_USER)
	echo "set global slow_query_log_file='/var/log/mysql/slow.log';" | mysql -p$(MYSQL_PASSWORD) -u$(MYSQL_USER)
	echo "set global long_query_time=$(MYSQL_LONG_QUERY_TIME);" | mysql -p$(MYSQL_PASSWORD) -u$(MYSQL_USER)
	echo "show variables like 'slow_query_log_file%'" | mysql -p$(MYSQL_PASSWORD) -u$(MYSQL_USER)
	echo "show variables like 'long_query_time%'" | mysql -p$(MYSQL_PASSWORD) -u$(MYSQL_USER)
	curl -X POST $(DISCORD_WEBHOOK_SLOW_QUERY) -H 'Content-Type: application/json' --data '{"content": "ﾌﾟﾁ(｡･･)σ [ON] \n Update slow query setting... \n (/σ0σ)ｳｨｯｽ!"}'

.PHONY: slow-off
slow-off:
	echo "set global slow_query_log=0;" | mysql -p$(MYSQL_PASSWORD) -u$(MYSQL_USER)
	curl -X POST $(DISCORD_WEBHOOK_SLOW_QUERY) -H 'Content-Type: application/json' --data '{"content": "(o’▽’)σ [OFF] \n Update slow query setting... \n o(__*)Zzz"}'

.PHONY: build
build:
	sudo nginx -s reload
	sudo service nginx reload
	cd $(WEBAPP_HOME); \
	docker compose -f docker-compose-go.yml down --rmi all --volumes --remove-orphans; \
	docker compose -f docker-compose-go.yml up -d
	curl -X POST $(DISCORD_WEBHOOK_CICD_LOG) -H 'Content-Type: application/json' --data '{"content": "デプロイ完了: $(shell git rev-parse HEAD)"}'

.PHONY: analyze
analyze:
	echo "pt-query-digest"
	sudo pt-query-digest /var/log/mysql/slow.log | tee digest.log
	aws s3 cp digest.log  s3://isucon12q-log-storage/slow-query/pt-query-digest_$(TIMESTAMP).log
	curl -X POST $(DISCORD_WEBHOOK_SLOW_QUERY) -H 'Content-Type: application/json' --data '{"content": "壁|]llllll´▽)つ どうぞ♪ \n $(AWS_S3_SLOW_QUERY_DIR)pt-query-digest_$(TIMESTAMP).log"}'
	echo "ALP"
	cat /var/log/nginx/access.log | alp ltsv | tee alp.log
	aws s3 cp alp.log s3://isucon12q-log-storage/access-log/alp_$(TIMESTAMP).log
	echo "clean up"
	rm digest.log alp.log
	curl -X POST $(DISCORD_WEBHOOK_ACCESS_LOG) -H 'Content-Type: application/json' --data '{"content": "壁|]llllll´▽)つ どうぞ♪ \n $(AWS_S3_ACCESS_LOG_DIR)alp_$(TIMESTAMP).log"}'

.PHONY: setup-ci
setup-ci:
	-@mkdir $(ISUCON_HOME)/gitlab-runner-workspace
	curl -L --output $(ISUCON_HOME)/gitlab-runner-workspace/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
	chmod +x $(ISUCON_HOME)/gitlab-runner-workspace/gitlab-runner
	cd $(ISUCON_HOME)/gitlab-runner-workspace; \
	sudo ./gitlab-runner install --user=isucon --working-directory=$(ISUCON_HOME)/gitlab-runner-workspace; \
	sudo ./gitlab-runner register -n --executor shell --tag-list "isucon12" --url https://gitlab.com/ --registration-token $(TOKEN) --description "shell executor for isucon12"
	sudo service gitlab-runner start
	mv $(ISUCON_HOME)/.bash_logout $(ISUCON_HOME)/trash.bash_logout

.PHONY: clean-ci
clean-ci:
	sudo service gitlab-runner stop
	cd $(ISUCON_HOME)/gitlab-runner-workspace; \
	sudo ./gitlab-runner uninstall
	sudo rm /etc/gitlab-runner/config.toml
	sudo rm -r -f $(ISUCON_HOME)/gitlab-runner-workspace
	mv $(ISUCON_HOME)/trash.bash_logout $(ISUCON_HOME)/.bash_logout

.PHONY: ci-deploy
ci-deploy:
	cd $(ISUCON_HOME); \
	git fetch; \
	git reset --hard $(CI_BUILD_REF); \
	make build

.PHONY: ci-n1lint
ci-n1lint:
	cd $(ISUCON_HOME); \
	chmod +x ./ci/n1lint.sh; \
	GO_APP_HOME="$(GO_APP_HOME)" AWS_S3_CI_LOG_DIR="$(AWS_S3_CI_LOG_DIR)" TIMESTAMP="$(TIMESTAMP)" DISCORD_WEBHOOK_CICD_LOG="$(DISCORD_WEBHOOK_CICD_LOG)" ./ci/n1lint.sh

.PHONY: bench
bench:
	cd $(ISUCON_HOME); \
	chmod +x ./ci/bench.sh; \
	NAME="${CI_BUILD_REF}" AWS_S3_CI_LOG_DIR="$(AWS_S3_CI_LOG_DIR)" TIMESTAMP="$(TIMESTAMP)" DISCORD_WEBHOOK_CICD_LOG="$(DISCORD_WEBHOOK_CICD_LOG)" ./ci/bench.sh